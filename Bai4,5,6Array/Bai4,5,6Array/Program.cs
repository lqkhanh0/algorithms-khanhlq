﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bai4_5_6Array
{
    internal class Program
    {

        // Câu 4
        static Product findProduct(List<Product> products, string name)
        {
            foreach (Product product in products)
            {
                if (product.name.Equals(name)) return product;
            }
            return null;
        }

        // Câu 5
        static List<Product> findId(List<Product> products, int categoryId)
        {
            List<Product> productlist = new List<Product>();

            foreach (Product product in products)
                if (product.categoryId == categoryId)
                {
                    productlist.Add(product);
                }
            return productlist;
        }
        // Câu 6
        static List<Product> findPrice(List<Product> products, int price)
        {
            List<Product> productlist = new List<Product>();

            foreach (Product product in products)
                if (product.price <= price)
                {
                    productlist.Add(product);
                }
            return productlist;
        }
        // Câu 11
        static List<Product> sortByPrice(List<Product> products)
        {
            List<Product> productlist = new List<Product>();
            for (int i = productlist.Count;i> 1; i--)
            {
                for (int j = 0; j < i-1; j++)
                {
                    if(productlist[j].price > productlist[j+1].price)
                    {
                        (productlist[j], productlist[j + 1]) = (productlist[j + 1], productlist[j]);
                    }
                }
            }
            foreach (Product product in products)
            {
                productlist.Add(product);
            }
            return productlist;
        }
        // Câu 12
        static List<Product> sortByName(List<Product> products)
        {
            List<Product> productlist = new List<Product>();
            for (int i = 1; i < productlist.Count; i++)
            {
                Product key = productlist[i];
                int j = i-1;
                while (j >= 0 && productlist[j].name.Length > key.name.Length)
                {
                    productlist[j+1] = productlist[j - 1];
                    j--;
                }
                productlist[j + 1] = key;
            }
            return productlist;
        }
        // Câu 15
        static List<Product> minByPrice(List<Product> products)
        {
            List<Product> productlist = new List<Product>();
            int minprice = 100;
            foreach (Product product in products)
            {
                if(minprice>= product.price)
                {
                    minprice = product.price;
                } 
            }
            foreach (Product product in products)
            {
                if(minprice == product.price)
                {
                    productlist.Add(product);
                }
            }
            return productlist;
        }
        // Câu 16
        static List<Product> maxByPrice(List<Product> products)
        {
            List<Product> productlist = new List<Product>();
            int maxprice = 0;
            foreach (Product product in products)
            {
                if (maxprice <= product.price)
                {
                    maxprice = product.price;
                }
            }
            foreach (Product product in products)
            {
                if (maxprice == product.price)
                {
                    productlist.Add(product);
                }
            }
            return productlist;
        }
        
        public static void Main()
        {
            List<Product> products = new List<Product>() {
            new Product() { name = "CPU", price = 750, quality = 10, categoryId = 1 },
            new Product() { name = "RAM", price = 50, quality= 2, categoryId = 2},
            new Product() { name = "HDD", price = 70, quality= 1, categoryId = 2},
            new Product() { name = "Main", price = 400, quality= 3, categoryId = 1},
            new Product() { name = "Keyboard", price = 30, quality=  8, categoryId = 4},
            new Product() { name = "Mouse", price = 25, quality=  50, categoryId = 4},
            new Product() { name = "VGA", price = 60, quality=  35, categoryId = 3},
            new Product() { name = "Moniter", price = 120, quality=  28, categoryId = 2},
            new Product() { name = "Case", price = 120, quality=  28, categoryId = 5},
            };
            List<Category> categories = new List<Category>() {
            new Category() { id = 1, name= "Comuter"},
            new Category() {id= 2, name= "Memory"},
            new Category() {id= 3, name= "Card"},
            new Category() {id= 4, name= "Acsesory"},
            };
            
            var list = (from pro in products
                       join cat in categories
                       on pro.categoryId equals cat.id
                       select new { pro.name, pro.price, pro.quality, cat.id /*, cat.name*/
    }).ToList();

            string productname = Console.ReadLine();
            Console.WriteLine(findProduct(products, productname));

            int categoryid = Convert.ToInt32(Console.ReadLine());
            List<Product> p = findId(products, categoryid);
            foreach (Product listproductCateId in p)
            {
                Console.WriteLine(listproductCateId.ToString());
            }

            int xprice = Convert.ToInt32(Console.ReadLine()); ;
            p = findPrice(products, xprice);
            foreach (Product listproductPrice in p)
            {
                Console.WriteLine(listproductPrice.ToString());
            }
            
            Console.WriteLine("Bubble sort:");
            p = sortByPrice(products);
            foreach (Product bubblesortlistbyPrice in p)
            {
                Console.WriteLine(bubblesortlistbyPrice.ToString());
            }

            Console.WriteLine("Insertion sort:");
            p = sortByName(products);
            foreach (Product insertionsortlistbyName in p)
            {
                Console.WriteLine(insertionsortlistbyName.ToString());
            }

            Console.WriteLine("Min price of product");
            p = minByPrice(products);
            foreach(Product minByPrice in p)
            {
                Console.WriteLine(minByPrice.ToString());
            }

            Console.WriteLine("Max price of product");
            p = maxByPrice(products);
            foreach (Product maxByPrice in p)
            {
                Console.WriteLine(maxByPrice.ToString());
            }
            Console.ReadLine();
        }

    }
}
