﻿using System;

namespace Bai22Recursion
{
    internal class Program
    {
        // Cách 1 
        static int calMonth(float money, float rate)
        {

            return (int)(calMonth(money, rate) / (1 + rate / 100));
        }
        // Cách 2
        //static int calMonth(float money, float rate)
        //{
        //    int count = 0;
        //    for (int i = 0; i < 1000; i++)
        //    {
        //        count++;
        //        if (Math.Pow((1 + rate / 100), i) >= 2)
        //            break;
        //    }
        //    return count - 1;
        //}
        static void Main(string[] args)
        {
            Console.WriteLine("Enter money: ");
            float money = float.Parse(Console.ReadLine());
            Console.WriteLine("Enter rate: ");
            float rate = float.Parse(Console.ReadLine());
            Console.WriteLine(calMonth(money, rate));
        }
    }
}
