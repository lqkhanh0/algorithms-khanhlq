﻿using System;

namespace Bai21Recursion
{
    internal class Program
    {
        // Cách 1
        //static float calSalary(float salary, int n)
        //{
        //    if (n == 1) return salary; 
        //    else
        //    {
        //        return (float) (calSalary(salary, n - 1) * 1.1);
        //    }
        //}
        // Cách 2
        static float calSalary(float salary, int n)
        {
            if(n == 1) return salary;
            else
                for(int i = 1; i < n; i++)
                {
                    salary = (float)(salary * 1.1);
                }
            return salary;
            
        }
        static void Main(string[] args)
        {
            Console.WriteLine("Enter salary: ");
            float salary = float.Parse(Console.ReadLine());
            Console.WriteLine("Enter number of year: ");
            int n = int.Parse(Console.ReadLine());
            Console.WriteLine(calSalary(salary,n));
        }
    }
}
